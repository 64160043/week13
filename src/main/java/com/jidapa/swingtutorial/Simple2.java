package com.jidapa.swingtutorial;

import javax.swing.*; // import javax.swing.JFrame; จะimportทีละตัวเลยก็ได้แต่ว่าสามารถimportทุกตัวทีเดียวได้เลยโดยการใช้สตาร์แบบบรรทัดนี้

class MyFrame extends JFrame {
    JButton button; // การย้ายออกมาไว้ข้างนอกคือการสร้างAttribute จะใส่หรือไม่ใส่ private ก็ได้

    public MyFrame() {
        super("First JFrame");
        this.setTitle("First JFrame");
        button = new JButton("Click");
        button.setBounds(130, 100, 100, 40);
        this.setLayout(null);
        this.add(button);
        this.setSize(400, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
}

public class Simple2 {
    public static void main(String[] args) {
        MyFrame frame = new MyFrame(); // บรรทัดนี้สามารถเปลี่ยนMyFameเป็นJframeได้
        frame.setVisible(true);
    }

}
