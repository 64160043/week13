package com.jidapa.swingtutorial;

import javax.swing.JButton;
import javax.swing.JFrame;

public class FristSwingExample {
    public static void main(String[] args) {

        JFrame frame = new JFrame(); // JFrame frame = new JFrame("First JFrame");แบบนี้ก็ได้หรือจะแบบข้างล่างก็ได้
        frame.setTitle("First JFrame");
        JButton button = new JButton("Click");
        button.setBounds(130, 100, 100, 40);
        frame.setLayout(null);
        frame.add(button);
        frame.setSize(400, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        

    }

}
