package com.jidapa.swingtutorial;

import javax.swing.JFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class HelloMyName extends JFrame {
    JLabel lblname;
    JTextField txtName;
    JButton btnHello;
    JLabel lblHello;

    public HelloMyName() {
        super("Hello My Name");
        lblname = new JLabel("Name: ");
        lblname.setBounds(10, 10, 50, 20);
        lblname.setHorizontalAlignment(JLabel.RIGHT);

        txtName = new JTextField();
        txtName.setBounds(70, 10, 200, 20);

        btnHello = new JButton("Hello");
        btnHello.setBounds(30, 40, 250, 20);
        btnHello.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String myName = txtName.getText();
                lblHello.setText("Hello " + myName);

            }
        });

        lblHello = new JLabel("Hello my name");
        lblHello.setHorizontalAlignment(JLabel.CENTER);
        lblHello.setBounds(30, 70, 250, 20);
        this.add(btnHello);
        this.add(lblname);
        this.add(txtName);
        this.add(lblHello);

        this.setLayout(null);
        this.setSize(400, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        HelloMyName frame = new HelloMyName();

    }

}
