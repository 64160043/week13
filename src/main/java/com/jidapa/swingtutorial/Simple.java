package com.jidapa.swingtutorial;

import javax.swing.*; // import javax.swing.JFrame; จะimportทีละตัวเลยก็ได้แต่ว่าสามารถimportทุกตัวทีเดียวได้เลยโดยการใช้สตาร์แบบบรรทัดนี้

class MyApp {
    JFrame frame;
    JButton button; //การย้ายออกมาไว้ข้างนอกคือการสร้างAttribute จะใส่หรือไม่ใส่ private ก็ได้

    public MyApp() {
        frame = new JFrame(); // JFrame frame = new JFrame("First JFrame");แบบนี้ก็ได้หรือจะแบบข้างล่างก็ได้
        frame.setTitle("First JFrame");
        button = new JButton("Click");
        button.setBounds(130, 100, 100, 40);
        frame.setLayout(null);
        frame.add(button);
        frame.setSize(400, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }
}

public class Simple {
    public static void main(String[] args) {
        MyApp app = new MyApp();
    }

}
